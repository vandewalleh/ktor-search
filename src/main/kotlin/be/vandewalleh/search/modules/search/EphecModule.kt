package be.vandewalleh.search.modules.search

import io.ktor.http.URLBuilder

class EphecModule : SearchModule {
    override val key: String
        get() = "e"

    private val default = "portal.ephec.be"

    private val bookmarks = mapOf(
            "portal" to default,
            "ca" to "campus-virtuel.ephec.be/sites/ephec2018",
            "per" to "eperso.ephec.be",
            "one" to "ephec-my.sharepoint.com",
            "os" to "github.com/melvinmajor/F1-of-Linux",
            "el" to "github.com/melvinmajor/thermopic",
            "to" to "github.com/melvinmajor/TodoList",
            "sa" to "github.com/melvinmajor/VPS-project",
            "in" to "inginious.ephec.be/courselist",
            "mo" to "moodle.ephec.be/my/index.php",
            "cal" to "outlook.office365.com/owa/?realm=ephec.be#path=/calendar",
            "mail" to "outlook.office365.com/owa/?realm=ephec.be#path=/mail",
            "cisco" to "www.netacad.com",
            "te" to "teams.microsoft.com",
            "cl" to "clockify.me/tracker",
            "tr" to "trello.com/b/XEX3CKmq/seed-it",
            "gg" to "github.com/seed-IT"
    )

    override fun buildUrl(urlBuilder: URLBuilder, args: List<String>) {
        urlBuilder.host = if (args.isEmpty() || !bookmarks.containsKey(args[0])) default
        else bookmarks.getValue(args[0])
    }

}