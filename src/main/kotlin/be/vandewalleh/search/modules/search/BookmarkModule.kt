package be.vandewalleh.search.modules.search

import io.ktor.http.URLBuilder

class BookmarkModule : SearchModule {
    override val key: String
        get() = "b"

    private val bookmarks = mapOf(
            "hn" to "news.ycombinator.com"
    )

    override fun buildUrl(urlBuilder: URLBuilder, args: List<String>) {
        if (args.isEmpty()) return

        val key = args[0]
        bookmarks[key]?.let { urlBuilder.host = it }
    }
}