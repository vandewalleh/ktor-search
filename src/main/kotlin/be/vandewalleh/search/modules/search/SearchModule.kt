package be.vandewalleh.search.modules.search

import io.ktor.http.URLBuilder

interface SearchModule {
    val key: String

    fun buildUrl(urlBuilder: URLBuilder, args: List<String>)
}

