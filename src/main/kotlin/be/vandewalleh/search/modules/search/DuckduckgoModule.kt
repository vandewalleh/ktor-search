package be.vandewalleh.search.modules.search

import io.ktor.http.URLBuilder

class DuckduckgoModule : SearchModule {
    override val key: String
        get() = "d"

    override fun buildUrl(urlBuilder: URLBuilder, args: List<String>) {
        urlBuilder.host = "duckduckgo.com"

        if (args.isNotEmpty()) {
            urlBuilder.parameters["q"] = args.joinToString("+")
        }
    }
}