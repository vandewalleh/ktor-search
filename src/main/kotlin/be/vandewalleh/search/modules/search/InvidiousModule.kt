package be.vandewalleh.search.modules.search

import be.vandewalleh.search.extensions.joinToWords
import io.ktor.http.URLBuilder

class InvidiousModule : SearchModule {
    override val key: String
        get() = "yt"

    private val knownChannels = mapOf(
            "se" to "UCu8YylsPiu9XfaQC74Hr_Gw",
            "bp" to "UCOpP5PqrzODWpFU961acUbg",
            "bk" to "UC3DFdy_qc-cqgKCyQTHLGzA",
            "ss" to "UCfUGBBnxQYezwJM9wi3F-Lg"
    )

    override fun buildUrl(urlBuilder: URLBuilder, args: List<String>) {
        urlBuilder.host = "invidio.us"

        if (args.isEmpty()) return

        val channel = knownChannels[args[0]]
        if (channel != null) {
            urlBuilder.encodedPath = "/channel/$channel"
        } else {
            urlBuilder.encodedPath = "/search"
            urlBuilder.parameters["q"] = args.joinToWords()
        }
    }
}