package be.vandewalleh.search.modules.search

import be.vandewalleh.search.extensions.joinToWords
import be.vandewalleh.search.extensions.skip
import io.ktor.http.URLBuilder

class RedditModule : SearchModule {
    override val key: String
        get() = "r"

    override fun buildUrl(urlBuilder: URLBuilder, args: List<String>) {
        urlBuilder.host = "old.reddit.com"

        if (args.isEmpty()) return

        urlBuilder.encodedPath = "/r/" + args[0]

        if (args.size < 2) return

        when (args[1]) {
            "ta" -> {
                urlBuilder.encodedPath += "/top"
                urlBuilder.parameters["sort"] = "top"
                urlBuilder.parameters["t"] = "all"
            }
            "tm" -> {
                urlBuilder.encodedPath += "/top"
                urlBuilder.parameters["sort"] = "top"
                urlBuilder.parameters["t"] = "month"
            }
            else -> {
                urlBuilder.encodedPath += "/search"
                urlBuilder.parameters["q"] = args.skip(1).joinToWords()
                urlBuilder.parameters["restrict_sr"] = "on"
            }
        }
    }
}