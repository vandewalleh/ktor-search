package be.vandewalleh.search.features

import be.vandewalleh.search.extensions.skip
import be.vandewalleh.search.modules.search.*
import io.ktor.application.call
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get


private val defaultModule: SearchModule = DuckduckgoModule()

private val searchModulesByKey = listOf(
        defaultModule,
        BookmarkModule(),
        InvidiousModule(),
        RedditModule(),
        EphecModule()
).associateBy { it.key }

fun Route.redirect() {
    get("/") {
        val q = call.parameters["q"]

        var urlBuilder = defaultUrlBuilder()

        if (q == null) {
            defaultModule.buildUrl(urlBuilder, emptyList())
            val url = urlBuilder.buildString()
            return@get call.respondRedirect(url, true)
        }

        val words = q.trim().split(" ")
        val key = words[0].substring(1) // skip "!" TODO: regex
        var args = words.skip(1)

        var searchModule = searchModulesByKey[key]
        if (searchModule == null) {
            searchModule = defaultModule
            args = words
        }

        searchModule.buildUrl(urlBuilder, args)

        if (urlBuilder.host.isEmpty()) {
            urlBuilder = defaultUrlBuilder()
            defaultModule.buildUrl(urlBuilder, args)
        }

        val url = urlBuilder.buildString()

        return@get call.respondRedirect(url, true)

    }
}

private fun defaultUrlBuilder() = URLBuilder(
        host = "",
        protocol = URLProtocol.HTTPS,
        port = URLProtocol.HTTPS.defaultPort
)
