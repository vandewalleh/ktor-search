package be.vandewalleh.search.features

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.toLogString
import io.ktor.request.uri
import org.slf4j.event.Level

fun Application.logging() {
    install(CallLogging) {
        level = Level.INFO
        format {
            "${it.request.uri} ${it.response.status()}: ${it.request.toLogString()}"
        }
    }
}