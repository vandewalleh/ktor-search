package be.vandewalleh.search.features

import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get

fun Route.ping() {
    get("/ping") {
        call.respondText("pong")
    }
}