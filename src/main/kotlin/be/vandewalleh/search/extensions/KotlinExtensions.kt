package be.vandewalleh.search.extensions

fun <T> Iterable<T>.joinToWords() = this.joinToString(" ")

fun <T> List<T>.skip(count: Int) = this.subList(count, this.size)