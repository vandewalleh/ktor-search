package be.vandewalleh.search

import be.vandewalleh.search.features.logging
import be.vandewalleh.search.features.ping
import be.vandewalleh.search.features.redirect
import be.vandewalleh.search.features.status
import io.ktor.application.Application
import io.ktor.routing.routing

fun Application.main() {
    status()
    logging()
    routing {
        redirect()
        ping()
    }
}